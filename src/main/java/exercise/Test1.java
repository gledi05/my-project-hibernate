package exercise;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test1 {
    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml").buildSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        Address address = new Address();
        address.setStreet("Rruga Dibres");
        address.setBuilding("Servete Maci");
        address.setAppNo(9);
        Customer customer = new Customer();
        customer.setName("Bobby");
        customer.setEmail("bob@gmail.com");
        customer.setPhone_number("856748678");
        customer.setAddress(address);
        Customer customer1 = new Customer();
        customer1.setName("John");
        customer1.setEmail("bob999@gmail.com");
        customer1.setPhone_number("856748678");
        customer1.setAddress(address);

        Product product1=new Product();
        product1.setName("Laps");
        product1.setPrice(100d);
        product1.setDesc("Shkruan");
        customer.setProducts(List.of(product1));
        customer1.setProducts(List.of(product1));
        session.persist(product1);
        session.persist(customer);
        session.persist(customer1);

    }
}
