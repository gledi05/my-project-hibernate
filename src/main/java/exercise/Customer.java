package exercise;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "Costumer")

public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int Id ;

    private String name;

    private String email ;

    private String Phone ;

    private Address address ;
    @ManyToMany
    @JoinTable(name = "CUSTOMER_PRODUCT",joinColumns =@JoinColumn(name = "CUSTOMER_ID"),
    inverseJoinColumns = @JoinColumn(name = "PRODUCT_ID"))
    private List<Product>products;

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return Phone;
    }

    public void setPhone_number(String Phone) {
        this.Phone = Phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}