package exercise;

import  jakarta.persistence.Embeddable;

@Embeddable
public class Address {
    private String street;
    private String building;
    private int appNo;
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getBuilding() {
        return building;
    }
    public void setBuilding(String building) {
        this.building = building;
    }
    public int getAppNo() {
        return appNo;
    }
    public void setAppNo(int appNo) {
        this.appNo = appNo;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", appNo=" + appNo +
                '}';
    }
}