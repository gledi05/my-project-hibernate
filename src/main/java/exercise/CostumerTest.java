package exercise;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import java.util.List;
public class CostumerTest {
    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml").buildSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        Address address = new Address();
        address.setStreet("Rruga Dibres");
        address.setBuilding("Servete Maci");
        address.setAppNo(9);
        Customer customer = new Customer();
        customer.setName("Bobby");
        customer.setEmail("bob@gmail.com");
        customer.setPhone_number("856748678");
        customer.setAddress(address);
        Customer customer1 = new Customer();
        customer1.setName("John");
        customer1.setEmail("bob999@gmail.com");
        customer1.setPhone_number("856748678");
        customer1.setAddress(address);
        session.persist(customer);
        session.persist(customer1);
        Customer found = session.find(Customer.class, 1);
        System.out.println("---------------------------------------");
        System.out.println("Found Customer");
        System.out.println(found);
        customer.setPhone_number("6344653");
        Customer updated = session.merge(customer);
        System.out.println("---------------------------------------");
        System.out.println("Updated Customer");
        System.out.println(updated);
        Query<Customer> query = session.createQuery("FROM Customer", Customer.class);
        List<Customer> all = query.getResultList();
        System.out.println("---------------------------------------");
        System.out.println("All customers");
        all.forEach(System.out::println);
        session.remove(customer);
        found = session.find(Customer.class, 1);
        System.out.println(found);
        query = session.createQuery("From Customer where name like '%John%'", Customer.class);
        List<Customer> ct = query.getResultList();
        System.out.println("---------------------------------------");
        System.out.println("All customers that have name John");
        ct.forEach(System.out::println);
        transaction.commit();
        session.close();
        sf.close();
    }
}