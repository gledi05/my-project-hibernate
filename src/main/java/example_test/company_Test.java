package example_test;

import Example.Movie;
import Example.ProductionCompany;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class company_Test {

    private static SessionFactory sessionFactory;

    public static void main(String[] args) {
        try {
            // Create the SessionFactory
            sessionFactory = new Configuration().configure().buildSessionFactory();

            // Create a new company object
            Company company = new Company();
            company.setName("ompany");
            company.setEmployees_total(100);
            company.setNetworth(30);
            Company company2=new Company();
            company2.setName("samsung");
            company2.setNetworth(500);
            company2.setEmployees_total(700);

            // Open a session
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            // Persist the company object
            session.persist(company);
            session.persist(company2);

            // Commit the transaction
            session.getTransaction().commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sessionFactory != null) {
                sessionFactory.close();
            }
        }
    }
}

