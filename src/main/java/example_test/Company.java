package example_test;

import jakarta.persistence.*;

@Entity
@Table(name = "Company")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "company_id")
    private int id;
    private String name;
    private int networth_Billion;
    private int employees_total;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNetworth() {
        return networth_Billion;
    }

    public void setNetworth(int networth_Billion) {
        this.networth_Billion = networth_Billion;
    }

    public int getEmployees_total() {
        return employees_total;
    }

    public void setEmployees_total(int employees_total) {
        this.employees_total = employees_total;
    }


    public Company(int id, String name, int networth_Billion, int employees_total) {
        this.id = id;
        this.name = name;
        this.networth_Billion = networth_Billion;
        this.employees_total = employees_total;
    }

    public Company() {
    }
}
