package Library;

import example_Student.Student;

import java.util.Optional;

public interface BookDAO {
    void createTable();
    void deleteTable();
    void createBook(Book book);
    void deleteBook(int id);
    void updateBook(Book book);
    public Optional<Book> findBookById(int id);
}
