package Library;

import jakarta.persistence.*;

@Entity
@Table(name = "Book") // Added table name annotation
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ISBN") // Added column name annotation
    private int id;

    @Column(name = "Title")
    private String title;

    @Column(name = "Publication_Year")
    private int year;

    @Column(name = "Author")
    private String author;

    public Book() {
    }

    public Book(String title, int year, String author) {
        this.title = title;
        this.year = year;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
// Getters and setters

    // No changes made in getter and setter methods
}
