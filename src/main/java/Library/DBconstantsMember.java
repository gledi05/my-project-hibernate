package Library;

public class DBconstantsMember {
    public static String CREATE_TABLE = "create table Member ("+
            "id int auto_increment primary key not null,"+
            "Name varchar(100) not null,"+
            "Expiry date not null);";
    public static  String url = "jdbc:mysql://localhost:3306/jdbc";
    public static String user = "root";
    public static String pass = "gledi";

    public static String DROP_TABLE = "drop table Member";
    public static String CREATE= "insert into Member ("+
            "Name,Expiry) values (?,?);";
    public static String DELETE = "delete from Member where id=?";
    public static String UPDATE = "update Member set Name=?,Expiry where id=?";
    public static String SELECT="select * from Member where id =?";
}
