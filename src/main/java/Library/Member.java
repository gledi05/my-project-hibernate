package Library;

import jakarta.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "Member") // Added table name annotation
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID") // Added column name annotation
    private int member_id;

    @Column(name = "Name")
    private String name;

    @Column(name = "expiry")
    private Date date;

    public Member() {
    }

    public Member(String name, Date date) {
        this.name = name;
        this.date = date;
    }

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
// Getters and setters

    // No changes made in getter and setter methods
}
