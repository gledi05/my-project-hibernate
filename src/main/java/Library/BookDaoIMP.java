package Library;

import example_Student.DbConstantsBooks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class BookDaoIMP implements BookDAO{
    private Connection connection;

    // No changes made
    public BookDaoIMP(Connection connection) {
        this.connection = connection;
    }

    // No changes made
    public BookDaoIMP() {

    }

    // Setter for the connection
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    @Override
    public void createTable() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.CREATE_TABLE)){
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteTable() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.DROP_TABLE)){
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createBook(Book book) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.CREATE)) {
          preparedStatement.setString(1,book.getTitle());
          preparedStatement.setInt(2,book.getYear());
          preparedStatement.setString(3,book.getAuthor());
          preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteBook(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.DELETE)) {
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateBook(Book book) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.UPDATE)) {
            preparedStatement.setInt(1,book.getId());
            preparedStatement.setString(2,book.getTitle());
            preparedStatement.setString(2,book.getAuthor());
            preparedStatement.setInt(2,book.getYear());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Book> findBookById(int id) {
        return Optional.empty();
    }
}
