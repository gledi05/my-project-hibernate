package Library;

import example_Student.DbConstantsBooks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class MemberDaoIMP implements MemberDAO{
    private Connection connection;

    // No changes made
    public MemberDaoIMP(Connection connection) {
        this.connection = connection;
    }

    // No changes made
    public MemberDaoIMP() {

    }

    // Setter for the connection
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    @Override
    public void createTable() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DBconstantsMember.CREATE_TABLE)){
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteTable() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DBconstantsMember.DROP_TABLE)){
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createMember(Member member) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.CREATE)) {
            preparedStatement.setString(1,member.getName());
            preparedStatement.setDate(2,member.getDate());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteMember(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DBconstantsMember.DELETE)) {
            preparedStatement.setInt(1,id);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateMember(Member member) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DBconstantsMember.UPDATE)) {
            preparedStatement.setInt(1,member.getMember_id());
            preparedStatement.setString(2, member.getName());
            preparedStatement.setDate(3,member.getDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Member> findMemberByID(int id) {
        return Optional.empty();
    }
}
