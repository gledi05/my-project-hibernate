package Library;

import example_Student.Student;

import java.util.Optional;

public interface MemberDAO {
    void createTable();
    void deleteTable();
    void createMember(Member member);
    void deleteMember(int id);
    void updateMember(Member member);
    public Optional<Member> findMemberByID(int id);
}
