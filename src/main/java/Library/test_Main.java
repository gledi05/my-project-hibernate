package Library;
 import org.hibernate.Session;
 import org.hibernate.SessionFactory;
 import org.hibernate.cfg.Configuration;

public class test_Main {
    private static SessionFactory sessionFactory;

    public static void main(String[] args) {
        try {
            // Create the SessionFactory
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory(); // Modified configuration file name

            // Open a session
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            // Create a new book object
            Book book = new Book("Gledi", 2022, "Van");

            // Persist the book object
            session.persist(book);

            // Commit the transaction
            session.getTransaction().commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sessionFactory != null) {
                sessionFactory.close();
            }
        }
    }
}

