package E_Albania;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {
    public static void main(String[] args) {
        SessionFactory sessionFactory=new Configuration()
                .configure("hibernate.cfg.xml").buildSessionFactory();
        Session session= sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        E_AlbaniaID eAlbaniaID=new E_AlbaniaID();
        E_AlbaniaID eAlbaniaID2=new E_AlbaniaID();
        eAlbaniaID.setEmail("bob@gmail.com");
        eAlbaniaID2.setEmail("bob@gmail.com");
        eAlbaniaID.setPersonalNO("12345");
        eAlbaniaID2.setPersonalNO("123456");

        EAlbania eAlbania=new EAlbania();
        EAlbania eAlbania1=new EAlbania();
        eAlbania1.seteAlbaniaID(eAlbaniaID);
        eAlbania.seteAlbaniaID(eAlbaniaID2);
        eAlbania1.setAge(33);
        eAlbania.setAge(33);
        eAlbania1.setName("Bob");
        eAlbania.setName("Bob");

        session.persist(eAlbania);
        session.persist(eAlbania1);
        transaction.commit();
    }
}
