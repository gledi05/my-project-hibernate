package E_Albania;

import jakarta.persistence.Embedded;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "E_Albania")
public class EAlbania {
    @EmbeddedId
    private E_AlbaniaID eAlbaniaID;
    private String name;
    private int age;

    public EAlbania(E_AlbaniaID eAlbaniaID, String name, int age) {
        this.eAlbaniaID = eAlbaniaID;
        this.name = name;
        this.age = age;
    }

    public E_AlbaniaID geteAlbaniaID() {
        return eAlbaniaID;
    }

    public void seteAlbaniaID(E_AlbaniaID eAlbaniaID) {
        this.eAlbaniaID = eAlbaniaID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public EAlbania() {
    }
}
