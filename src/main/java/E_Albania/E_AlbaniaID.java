package E_Albania;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class E_AlbaniaID implements Serializable {
private String email;
private String PersonalNO;

    public E_AlbaniaID() {
    }

    public E_AlbaniaID(String email) {
        this.email = email;
    }

    public E_AlbaniaID(String email, String personalNO) {
        this.email = email;
        PersonalNO = personalNO;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonalNO() {
        return PersonalNO;
    }

    public void setPersonalNO(String personalNO) {
        PersonalNO = personalNO;
    }
}
