package example_Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class StudentDaoIMP implements StudentDAO {
    private Connection connection;

    // No changes made
    public StudentDaoIMP(Connection connection) {
        this.connection = connection;
    }

    // No changes made
    public StudentDaoIMP() {

    }

    // Setter for the connection
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void createTable() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.CREATE_TABLE)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteTable() {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.DROP_TABLE)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createStudent(Student student) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.CREATE)) {
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getSurname());
            preparedStatement.setInt(3, student.getYearOfStart());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteStudent(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DbConstantsBooks.DELETE)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Student> findStudentById(int id) {
        // Implementation for finding a student by ID
        return Optional.empty();
    }
}
