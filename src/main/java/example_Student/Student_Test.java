package example_Student;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import java.sql.Connection;
import java.sql.SQLException;

public class Student_Test {
    private static SessionFactory sessionFactory;

    public static void main(String[] args) {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
            StudentDaoIMP studentDao = new StudentDaoIMP();

            // Open a session
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            Connection connection = null;
            try {
                connection = session.getSessionFactory().getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            studentDao.setConnection(connection);

            Student student1 = new Student();
            student1.setName("Gledi");
            student1.setSurname("Nezha");
            student1.setYearOfStart(2020);

            studentDao.createStudent(student1);

            // Commit the transaction
            session.persist(student1);
            session.getTransaction().commit();

            session.close();
            sessionFactory.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sessionFactory != null) {
                sessionFactory.close();
            }
        }
    }
}
