package example_Student;

import jakarta.persistence.*;

@Entity
@Table(name = "Student_Info")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "student_ID")
    private int ID;
    private String name;
    private String surname;
    private int yearOfStart;

    // No changes made
    // Getters and setters

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYearOfStart() {
        return yearOfStart;
    }

    public void setYearOfStart(int yearOfStart) {
        this.yearOfStart = yearOfStart;
    }


    public Student() {
    }

    // Constructor with parameters
    public Student(String name, String surname, int yearOfStart) {
        this.name = name;
        this.surname = surname;
        this.yearOfStart = yearOfStart;


        // Default constructor



    }
}