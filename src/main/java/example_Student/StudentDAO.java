package example_Student;

import java.util.Optional;
                             //Kjo perdorej per te krijuar metoda qe mund te thriteshin te nje class me implement
                             //dhe nga klasa qe u ben ben implement thirreshin ne main
public interface StudentDAO {
        void createTable();
        void deleteTable();
        void createStudent(Student student);
        public void deleteStudent(int id);

        public Optional<Student> findStudentById(int id);
    }
