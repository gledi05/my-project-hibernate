package Example;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "Production_Company")

public class ProductionCompany {
    public ProductionCompany(String name, int foundedYear,int id) {
        this.id=id;
        this.name = name;
        this.foundedYear = foundedYear;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "COMPANY_ID")
   private String name;
   @Column(name = "COMPANY_FOUNDED_YEAR")
   private int foundedYear;

   @OneToMany
    private List<Movie>movieList;
}


