package Example;

import jakarta.persistence.*;

@Entity
@Table(name = "MOVIE_DETAILS")
public class movieDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DETAILS_ID")
    private int id;

    @Column(name = "DIRECTOR_NAME")
    private String directorName;

    @Column(name = "BUDGET")
    private Double budget;

    @OneToOne
    @JoinColumn(name = "MOVIE_ID")
    private Movie movie;

    // Getters and Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}