package Example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class movieTest {
    public static void main(String[] args) {
        SessionFactory sessionFactory=new Configuration()
                .configure("hibernate.cfg.xml").buildSessionFactory();
        Session session= sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        ProductionCompany productionCompany=new ProductionCompany("Star",2000,11);
        Movie movie1=new Movie("Fast 1","Action",2023);
        Movie movie2=new Movie("Fast 2","Action",2023);
        Movie movie3=new Movie("Fast 3","Action",2023);
        session.persist(movie1);
        session.persist(movie2);
        session.persist(movie3);
        transaction.commit();
    }
}
