package Example;

import jakarta.persistence.*;

@Entity
@Table(name = "MOVIE_INFO")
public class Movie {
    public Movie() {
    }

    public Movie(int id, String title, String genre, int year) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.year = year;
    }

    public Movie(String title, String genre, int year) {
        this.title = title;
        this.genre = genre;
        this.year = year;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "movie_id")
    private int id;

    @Column(name = "movie_title")
    private String title;

    @Column(name = "movie_genre")
    private String genre;

    @Column(name = "year_of_release")
    private int year;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private ProductionCompany productionCompany;

    @OneToOne(mappedBy = "movie")
    private Example.movieDetails movieDetails;

    // Getters and Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public ProductionCompany getProductionCompany() {
        return productionCompany;
    }

    public void setProductionCompany(ProductionCompany productionCompany) {
        this.productionCompany = productionCompany;
    }

    public Example.movieDetails getMovieDetails() {
        return movieDetails;
    }

    public void setMovieDetails(Example.movieDetails movieDetails) {
        this.movieDetails = movieDetails;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", year=" + year +
                '}';
    }
}